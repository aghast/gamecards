# python: encoding=utf-8
""" aghast_utils.py

    Utility functions & classes.
"""
#import _ast     # pylint: disable=wrong-import-order
import ast
import collections
import configparser
import os

from contextlib import suppress
from typing import Any, Callable, MutableMapping, Optional, TYPE_CHECKING

import entrypoints as ep
import toml

from public import private, public


if TYPE_CHECKING:
    any_opt = Optional[Any]
    bool_opt = Optional[bool]
    bytes_opt = Optional[bytes]
    float_opt = Optional[float]
    int_opt = Optional[int]
    str_opt = Optional[str]
    Opt = Optional
public(
    any_opt=Optional[Any],
    bool_opt=Optional[bool],
    bytes_opt=Optional[bytes],
    float_opt=Optional[float],
    int_opt=Optional[int],
    str_opt=Optional[str],
    Opt=Optional,
)
del Optional


@private
def entrypoint_in_pkg_resources(ep_name: str, f_name: str, m_name: str) -> bool:
    """ Return True if an entrypoint in pkg_resources mentions a function.

        This will happen if the package has been installed (with the
        entrypoint correctly configured).

    """
    return any(
        e.object_name == f_name and e.module_name == m_name
        for e in ep.get_group_all(ep_name)
    )


EntryPointInfo = collections.namedtuple("EntryPointInfo", "name module item extra")


@private
def parse_entrypoint(epstr: str) -> EntryPointInfo:
    """ Parse an entrypoint record into a named tuple of its fields.

        Takes a line of the form:

            name = module:item [ '[' extra ']' ]

        (That is, the "[extra]" is optional.)

    """
    name, _, rhs = epstr.partition("=")
    name = name.strip()
    if not rhs:
        raise ValueError(f"Invalid entrypoint record: {epstr}")
    module, _, itemextra = rhs.partition(":")
    module = module.strip()
    if not itemextra:
        raise ValueError(f"Invalid entrypoint record: {epstr}")
    item, _, extra = itemextra.partition("[")
    item = item.strip()
    if extra:
        extra, _, _ = extra.partition("]")
        extra = extra.strip()

    return EntryPointInfo(name, module, item, extra)


@private
def entrypoint_in_pyproject_toml(ep_name: str, f_name: str, m_name: str) -> bool:
    """ Return True if an entrypoint in pyproject.toml mentions a function.

        This will happen if a tool entry contains the reference.

    """
    pyproj: MutableMapping[str, Any] = {"tool": {}}
    with suppress(FileNotFoundError), open("pyproject.toml") as f:
        pyproj = toml.load(f)

    for tool, tool_info in pyproj["tool"].items():
        if "entrypoints" in tool_info:
            for k, v in tool_info["entrypoints"].get(ep_name, {}).items():
                epi = parse_entrypoint(v)
                if epi.module == m_name and epi.item == f_name:
                    return True
    return False


@private
def entrypoint_in_setup_cfg(ep_name: str, f_name: str, m_name: str) -> bool:
    """ Return True if an entrypoint in setup.cfg mentions a function.

        This will happen if the 'options.entry_points' entry contains the
        reference.

    """
    config = configparser.ConfigParser()
    with suppress(FileNotFoundError):
        config.read("setup.cfg")

    opt_ep = config.get("options.entry_points", ep_name, fallback="")
    lines = opt_ep.splitlines()
    for line in lines:
        epi = parse_entrypoint(line)
        if epi.module == m_name and epi.item == f_name:
            return True
    return False


@private
def entrypoint_in_setup_py(ep_name: str, f_name: str, m_name: str) -> bool:
    """ Return True if an entrypoint in setup.py mentions a function.

        This will happen if the 'options.entry_points' entry contains the
        reference.

    """
    try:
        with open("setup.py") as f:
            setup = f.read()
    except FileNotFoundError:
        return False

    node = ast.parse(setup, filename="setup.py")

    class SetupVisitor(ast.NodeVisitor):
        def __init__(self):
            self.found = False

        def visit(self, node):
            try:
                if not isinstance(node, ast.Call) or node.func.id != "setup":
                    return super().visit(node)
                call_ep = [n for n in node.keywords if n.arg == "entry_points"][0]
                for k, v in zip(call_ep.value.keys, call_ep.value.values):
                    if not isinstance(k, ast.Str) or k.s != ep_name:
                        continue
                    if not isinstance(v, ast.List):
                        raise ValueError(
                            "Unexpected value of type "
                            f"{type(v)} in entrypoints[{k.s}]"
                        )
                    for elt in v.elts:
                        epi = parse_entrypoint(elt.s)
                        if epi.module == m_name and epi.item == f_name:
                            self.found = True
                            break
            except (AttributeError, IndexError):
                pass
            return super().visit(node)

    sv = SetupVisitor()
    sv.visit(node)
    return sv.found


@public
def entry_point(ep_name: str, check: bool = False, check_env: str_opt = ""):
    """ Confirm that a function is referenced by an entry point.

            @entry_point('console_scripts')
            def main():
                ...

        Given an entry-point name ('console_scripts', above), search
        for entry-point definitions in various places:

          * In the data managed by the pkg_resources module;
          * In the pyproject.toml file, if one exists;
          * In the setup.cfg file, if one exists;
          * In the parameters to a call to setup() in setup.py; if that
            exists.

        If any of these are found (they are searched in the order
        shown), the requirement is satisfied and nothing happens. If
        none are found, raise an exception.

    """

    if not check and check_env:
        envar = os.environ.get(check_env, "False").lower()
        check = envar in "1 On True Yes".lower().split()

    def decorator(func: Callable) -> Callable:
        if not check:
            return func

        f_name = func.__name__
        m_name = func.__module__

        if any(
            check(ep_name, f_name, m_name)
            for check in (
                entrypoint_in_pkg_resources,
                entrypoint_in_pyproject_toml,
                entrypoint_in_setup_cfg,
                entrypoint_in_setup_py,
            )
        ):
            return func

        raise ValueError(f"No reference to {f_name} found for entry point {ep_name}")

    return decorator


@public
class ObjectDict(dict):
    """ A dictionary whose keys are accessible as attributes.

        See: https://goodcode.io/articles/python-dict-object/

    """
    def __getattr__(self, attr_name: str) -> Any:
        """ Fetch an attribute's value from the underlying dict.

            See: https://docs.python.org/3/reference/datamodel.html#object.__getattr__

        """
        if attr_name in self:
            return self[attr_name]
        else:
            raise AttributeError(f"No such attribute: '{attr_name}'")

    def __setattr__(self, attr_name: str, value: Any) -> None:
        """ Store an attribute's value in the underlying dict.

            See:https://docs.python.org/3/reference/datamodel.html#object.__setattr__

        """
        if hasattr(self, attr_name):
            super().__setattr__(attr_name, value)
        else:
            self[attr_name] = value


    def __delattr__(self, attr_name: str):
        """ Delete an attribute from the object, or the underlying dict.

            See:https://docs.python.org/3/reference/datamodel.html#object.__delattr__

        """
        if attr_name in self:
            del self[attr_name]
        elif hasattr(self, attr_name):
            super().__delattr__(attr_name)
        else:
            raise AttributeError(f"No such attribute: '{attr_name}'")
