#! /usr/bin/env python
""" A setup.py shim for use with pyproject.toml and setup.cfg.

"""
import setuptools

if __name__ == "__main__":
    setuptools.setup()
