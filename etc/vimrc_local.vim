" Project directory is ABOVE this one. ($PROJ/etc/vimrc_local.vim)
let PROJ_DIR = expand('<sfile>:p:h:h')
let PROJ_NAME = fnamemodify(PROJ_DIR, ':t')
let proj_dir = fnameescape(PROJ_DIR)
let proj_name = fnameescape(PROJ_NAME)

execute 'cd! '.proj_dir

" The &path variable is used to search for files using various load-file
" commands (including my own "find-file-if-not-exist" handling when Vim is
" first loaded). Python packages frequently have the same name as the 
" project, so looking for foo/foo/foo.py makes sense. Likewise, looking
" for foo/test/some_test.py" makes sense, so add "test" or "tests" to
" the &path.
" setlocal path<      " Create a local copy of &path from global copy
let &path = ".," . proj_dir . "/**"

" The &tags variable is used to locate tagfiles.
execute 'setlocal tags=./tags,'.proj_dir.'/tags,./tags'

let b:syntastic_checkers = ['python', 'mypy']

" When a command is entered by :foo<CR>, on the <CR>, check the command
" and reject ":q"
cnoremap <expr> <CR> getcmdtype() == ":" && index(["q", "W"], getcmdline()) >= 0 ? "<C-u>" : "<CR>"
