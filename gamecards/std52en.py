# python: encoding=utf-8
""" gamecards/std52en.py

    Provide card data for a generic "Standard 52-card English pattern" pack.

"""
import sys
from .gamecards import CardShape, GameCardABC, PackInfo, register_packinfo

from aghast_utils import entry_point

def suited_playing_card(info: PackInfo, card_id: str) -> GameCardABC:
    """ Construct a Game Card from pack info and card id. """



@entry_point('register_packinfo', check=True, check_env="ENTRY_POINT_CHECKS")
def register():
    """ Register pack with gamecards. """
    pi = PackInfo(
        title="Standard 52-card English pattern",
        pattern="English",
        suits="French",
        has_corner_index="True",
        height_mm=89,
        width_mm=64,
        shape=CardShape.ROUNDED_RECTANGLE,
        cards="""
            AC 2C 3C 4C 5C 6C 7C 8C 9C TC JC QC KC
            AD 2D 3D 4D 5D 6D 7D 8D 9D TD JD QD KD
            AH 2H 3H 4H 5H 6H 7H 8H 9H TH JH QH KH
            AS 2S 3S 4S 5S 6S 7S 8S 9S TS JS QS KS
            J1 J2
        """.split(),
        _factory=suited_playing_card,
    )
    register_packinfo(pi)
