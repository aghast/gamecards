# python: encoding=utf-8
""" gamecards/__init__.py

    .. Implementation file for a package directory. See
    https://docs.python.org/3/reference/import.html#regular-packages for
    details.

    .. py:module:: gamecards

    # NAME

    ``gamecards`` — represent game cards in Python

    # SYNOPSIS

    The ``gamecards`` module provides extensible information-holding and
    identification for modeling the paper **cards** that appear in various
    *playing card games,* *board games,* *collectible card games,*
    *trading card games,* and other "card games" of all types.

    The basic unit of organization in ``gamecards`` is the
    :class:`Pack`. An instance of ``Pack`` models a real-world printed
    **pack of cards,** and as such will hold information like size, shape,
    colors, copyright, publication, etc. In addition, each ``Pack`` will
    enumerate exactly the cards it contains.

    Certain packs of cards, including what are commonly called **playing
    cards,** are divided into subgroups like *court cards* (also known
    as *face cards*), and *suits.* For specific example, the card called
    the "Ace of Spades" in an English-suited pack is a *pip card* with
    one pip in the suit of "Spades." The Spanish-suited "Rey de Oros"
    is a court card in the suit of "Oros." The German-suited
    "Eichelass" is a pip card (Ace) with *two* (!!) pips in the suit of
    "Eichel". Finally, for Tarock packs there is another suit, called
    *trumps,* that are traditionally marked with roman numbers. The "I"
    card is sometimes called "The Magician." None of the trumps are
    court cards or pip cards.

    None of these divisions are represented structurally by the
    ``gamecards`` module. There is no ``Suit`` class, for example.
    However, because it represents a large fraction of use cases,
    ``gamecards`` does actually differentiate between ``GameCard``
    and ``PlayingCard`` classes, with a standard set of additional
    attributes present on the latter. Note that both classes are capable
    of supporting arbitrary information storage, so it's certainly
    possible to create ``GameCard`` instances with pips or suits. But if
    you're going to play Poker, you should have a hand full of
    ``PlayingCard`` objects.

"""
from .gamecards import *

__all__ = """
    CardShape
    GameCardABC
    PlayingCardABC
    PackInfo
    register_packinfo
    select
""".strip().split()
""" Public interface of this package. See https://www.python.org/dev/peps
    /pep-0008/#public-and-internal-interfaces
"""
__version__ = "0.1.0"
