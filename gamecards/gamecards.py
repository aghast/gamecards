# python: encoding=utf-8
""" gamecards/gamecards.py
"""
import sys
import collections
import enum
import json
from abc import ABC, abstractmethod
from contextlib import suppress
from typing import Any, Dict, Hashable, List, Mapping
import entrypoints as ep
from aghast_utils import ObjectDict, any_opt, int_opt, str_opt, public, private


@public
@enum.unique
class CardShape(enum.Enum):
    """ Enumerate the possible shapes of game cards. """
    CIRCLE = "Circle"
    OVAL = "Oval"
    RECTANGLE = "Rectangle"
    ROUNDED_RECTANGLE = "RoundedRectangle"
    SQUARE = "Square"
    TRIANGLE = "Triangle"


@public
class GameCardABC(ABC):
    """ Abstract base class for game cards. """
    @property
    @abstractmethod
    def height_mm(self) -> float:
        """ Return the height in millimeters of the card(s). (You should
            expect these values to be identical across all cards from
            the same pack in regular playing cards, otherwise it would
            be easy to cheat. Other games may have different sized
            cards, however.)

        """
        ...

    @property
    @abstractmethod
    def width_mm(self) -> float:
        """ Return the width in millimeters of the card(s). (You should
            expect these values to be identical across all cards from
            the same pack in regular playing cards, otherwise it would
            be easy to cheat. Other games may have different sized
            cards, however.)

        """
        ...

    @property
    @abstractmethod
    def card_shape(self) -> CardShape:
        """ Identify the overall shape of the card. """
        ...

    @property
    @abstractmethod
    def index(self) -> str:
        """ Return the index of the card, a short string that summarizes
            its identity. For example, "9S".

        """
        ...


class PlayingCardABC(GameCardABC):
    """ Represent a single playing card used in common playing card
        games (not "trading card" games, or "collectible card" games).

    """
    @property
    @abstractmethod
    def has_corner_index(self) -> bool:
        """ Return true if these cards have corner indices. (Squeezers).
            This is true for most modern packs, but not true for 19th
            century and before.

        """
        ...

    @property
    @abstractmethod
    def is_ace(self) -> bool:
        """ Return true if card is consider an Ace in this system. (Note
            that for example German cards use 2-pip cards as the Ace.)

        """
        ...

    @property
    @abstractmethod
    def is_court_card(self) -> bool:
        """ Return true if card is a court card in a non-trumps suit.
            (King, Queen, etc.)

        """
        ...

    @property
    @abstractmethod
    def is_joker(self) -> bool:
        """ Return true if card is a joker. """
        ...

    @property
    @abstractmethod
    def is_trumps(self) -> bool:
        """ Return true if card is a trumps (tarock) suited card. """
        ...

    @property
    @abstractmethod
    def pips(self) -> int:
        """ Number of pips on card, or 0 for court cards. """
        ...

    @property
    @abstractmethod
    def suit_name(self) -> bool:
        """ Name of card's suit, like "Spades" or "Crescents". """
        ...

    @property
    @abstractmethod
    def rank_name(self) -> bool:
        """ Name of card's rank, like "Ace" or "Queen". """
        ...


@private
def call_entrypoint_functions(ep_name: str) -> None:
    for ent in ep.get_group_all(ep_name):
        fn = ent.load()
        fn()


_PACK_INFO: List['PackInfo'] = []


@public
class PackInfo(ObjectDict):
    """ Card 'pack' (en-US: 'deck') metadata store.

        This class stores data "about" a particular pack of game cards.
        The data may include things like publisher, copyright date,
        designer, etc.

        All PackInfo must provide a "pack()" method that returns a
        `Pack` object. The easiest way is to add a key "pack" that maps
        to a class-type value with a 0-args constructor.

    """
    def __str__(self) -> str:
        return ("PackInfo{"
                + ", ".join(f"{str(k)}: {repr(v)}"
                            for k, v in self.items())
                + "}")

    def __repr__(self) -> str:
        return str(self)

    def matches(self, validators: Mapping[str, Any]) -> bool:
        """ Return true if all validators match. """
        def match(key: str, validator: any_opt) -> bool:
            """ Try to match a validator against an attribute value. The value
                can be anything, while the validator should be a string, a
                regular expression, or a number.

                I have chosen to pass any validation that has no comparison.
                Either if the validator is None, or if the attribute's value
                is None. I think this allows broader search results. If need
                be, it can be changed pretty easily.

            """
            print(f"Match self={self}, key={key}, validator={validator}")
            if key not in self or validator is None:
                return True
            value = self[key]
            with suppress(AttributeError):
                m = validator.search(value)     # Could be a regex
                return m is not None
            with suppress(TypeError):
                return validator in value       # Could be a substring
            if isinstance(validator, int):
                with suppress(TypeError):
                    return validator == int(value)
            if isinstance(validator, float):
                with suppress(TypeError):
                    return validator == float(value)
            with suppress(ValueError):
                return validator == value       # Could be an arbitrary value
            return False

        return all(match(k, v) for k, v in validators.items())


@public
def register_packinfo(pi: PackInfo) -> None:
    """ Add PackInfo record to collection, if it is not already present. """
    if all(pack.title != pi.title for pack in _PACK_INFO):
        print(f"Adding to _PACK_INFO ({len(_PACK_INFO)})")
        _PACK_INFO.append(pi)


@private
def register_plugins():
    """ Call the entrypoints to register packinfo data. """
    print("Hello, packinfo!")
    call_entrypoint_functions('register_packinfo')
    # if not _PACK_INFO:
    #     from .std52en import register
    #     register()


@public
def select(**kwargs: Any) -> List[PackInfo]:
    """ Locate and return a factory for cards, matching on various attributes. """
    if not _PACK_INFO:
        register_plugins()
    return [pi for pi in _PACK_INFO if pi.matches(kwargs)]


if False:

    _ATTRS = "cardinfo_ identity_"
    _CARDINFO: Dict = {}
    _GAMECARD_INFO = {}

    class GameCard(GameCardABC, collections.namedtuple("GameCardNT", _ATTRS)):
        """ Basic game card instance class. """

        # {{{

        def cardinfo(self, name: str) -> Any:
            """ Look up card info in remote data store. """
            return self.cardinfo_(self.identity_, name)

        @property
        def height_mm(self) -> float:
            """ Return the height in millimeters of the card(s). (You should
                expect these values to be identical across all cards from the
                same pack in regular playing cards, otherwise it would be easy
                to cheat. Other games may have different sized cards, however.)
            """
            return self.cardinfo("height_mm")

        @property
        def width_mm(self) -> float:
            """ Return the width in millimeters of the card(s). (You should
                expect these values to be identical across all cards from the
                same pack in regular playing cards, otherwise it would be easy
                to cheat. Other games may have different sized cards, however.)
            """
            return self.cardinfo("width_mm")

        @property
        def card_shape(self) -> "CardShape":
            """ Identify the overall shape of the card. """
            return self.cardinfo("card_shape")

        @property
        def index(self) -> str:
            """ Return the index of the card, a short string that summarizes its
                identity. For example, "9S".
            """
            return self.cardinfo("index")

        # }}}


    class PlayingCard(PlayingCardABC, GameCard):
        """ Basic playing card instance class. """
        @property
        def has_corner_index(self) -> bool:
            """ Return true if these cards have corner indices. (Squeezers). This
                is true for most modern packs, but not true for 19th century
                and before.
            """
            return self.cardinfo("has_corner_index")

        @property
        def is_ace(self) -> bool:
            """ Return true if card is consider an Ace in this system. (Note that
                for example German cards use 2-pip cards as the Ace.)
            """
            return self.cardinfo("is_ace")

        @property
        def is_court_card(self) -> bool:
            """ Return true if card is a court card in a non-trumps suit. (King,
                Queen, etc.)
            """
            return self.cardinfo("is_court_card")

        @property
        def is_joker(self) -> bool:
            """ Return true if card is a joker. """
            return self.cardinfo("is_joker")

        @property
        def is_trumps(self) -> bool:
            """ Return true if card is a trumps (tarock) suited card. """
            return self.cardinfo("is_trumps")

        @property
        def pips(self) -> int:
            """ Number of pips on card, or 0 for court cards. """
            return self.cardinfo("pips")

        @property
        def suit_name(self) -> bool:
            """ Name of card's suit, like "Spades" or "Crescents". """
            return self.cardinfo("suit_name")

        @property
        def rank_name(self) -> bool:
            """ Name of card's rank, like "Ace" or "Queen". """
            return self.cardinfo("rank_name")


    def pack_factory(info: dict, card_class=PlayingCard) -> List[PlayingCardABC]:
        """ Build and return a pack of cards. """

        pack = [PlayingCard(cid) for cid in card_ids]
        return pack

    def load_item(data):
        """ Install items from a dictionary that was loaded from JSON. """
        for k, v in data.items():
            if k not in _GAMECARD_INFO:
                data[k] = v


    def load_common(filespec: str):
        data = load_json_file(filespec)
        _GAMECARD_INFO.update(data)


    def load_json_file(filespec: str):
        with open(filespec) as f:
            data = json.load(f)
        return data



    def register_pack(info: PackInfo):
        """ Add a pack to _PACK_INFO for searching by find(). """
        if info not in _PACK_INFO:
            _PACK_INFO.append(info)


    def load_pack():
        """ Load a pack from JSON file and register it. """
        def make_cardinfo(*infos):
            def cardinfo_function(attr_name):
                for info in infos:
                    if attr_name in info:
                        return info[attr_name]
                return None

            return cardinfo_function

        packfile = "std52en.json"
        load_common(packfile)
        # Now create a pack
        deck_key = "deck.std52en"
        if deck_key not in _GAMECARD_INFO:
            raise Exception("Missing deck key: " + deck_key)
        deck_info = _GAMECARD_INFO[deck_key]
        deck_id = deck_info["id"]
        base_id = deck_id[5:]
        suits = deck_info["suits"].split()
        factory_data = {}

        for suit_name in suits:
            print("suit name:", suit_name)
            suit_id = f"suit.{base_id}.{suit_name}"
            assert suit_id in _GAMECARD_INFO
            suit_info = _GAMECARD_INFO[suit_id]
            cards = suit_info['cards']
            for card_name in cards.split():
                card_id = f"card.{base_id}.{card_name}"
                assert card_id in _GAMECARD_INFO
                card_info = _GAMECARD_INFO[card_id]
                factory_data[card_id] = make_cardinfo(card_info, suit_info, deck_info)
        # deck_id -> factory_data, Need to put deck info into registry?
        pi = PackInfo(**deck_info)
        register_pack(pi)


    if __name__ == "__main__":
        load_pack()
        while True:
            s = input(">>> ").strip()
            if s.lower().startswith("q"):
                break

            try:
                key, value= s.split('=')
                results = find(**{key.strip(): value.strip()})
                print(results)
            except Exception as e:
                print(e)
