# python: encoding=utf-8
""" ./test_gamecards.py

    Initial py.test test cases.
"""
import re

from .context import pytest
from .context import gamecards

DSOTM = gamecards.PackInfo(
    title="The Dark Side of the Moon",
    copyright_year="2020",
    publisher="Pink Floyd & Sons",
)

def test_packinfo_matches():
    assert DSOTM.matches({'title': 'Side'})         # string in string
    assert DSOTM.matches({'copyright_year':2020})   # int matches string
    pre = re.compile(r"Fl.*ns")
    assert DSOTM.matches({'publisher': pre})        # regex matches string


def test_packinfo_str_and_repr():
    s = str(DSOTM)
    r = repr(DSOTM)

    assert s == r
    assert s == "PackInfo{title: 'The Dark Side of the Moon', " \
                "copyright_year: '2020', " \
                "publisher: 'Pink Floyd & Sons'}"


def test_find_52card_english():
    packs = gamecards.select(pattern='English')
    assert isinstance(packs, list)
    assert len(packs) == 1
    std52 = packs[0]
    assert isinstance(std52, gamecards.PackInfo)
    assert std52.title == "Standard 52-card English pattern"
    assert len(std52.cards) == 54
