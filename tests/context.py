# python: encoding=utf-8
""" tests/context.py

    Set up testing context. You should be able to relatively import this module
    from another tests/ module, and get going:

        from .context import <modulename>
"""
import sys

from os.path import abspath, dirname, join

sys.path.insert(0, abspath(join(dirname(__file__), '..')))

del abspath, dirname, join, sys

import pytest
import gamecards
